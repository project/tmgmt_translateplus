TMGMT translate plus (tmgmt_translateplus)
------------------------------------------

TMGMT translate plus is a translator plugin for Translation Management Tool, 
providing full integration with the translate plus service 
(http://www.translateplus.com).


FEATURES
--------
  * Submit translation jobs to translate plus
  * Select the translation quality (Human or Machine translation)
  * Allow large translation jobs to be split into smaller parts for batch 
    submission (using sequential or entity-based methods)
  * Collection of completed jobs using either cron or callback mechanism


REQUIREMENTS
------------

Depends on Translation Management Tool (http://drupal.org/project/tmgmt).  
Registration on http://www.translateplus.com required to use this module.
