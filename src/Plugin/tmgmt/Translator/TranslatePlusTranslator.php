<?php

/**
 * @file
 * Contains \Drupal\tmgmt_translateplus\Plugin\tmgmt\Translator\TranslatePlusTranslator.
 */

namespace Drupal\tmgmt_translateplus\Plugin\tmgmt\Translator;

use Drupal;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Site\Settings;
use Drupal\Core\Url;
use Drupal\Core\Database\Connection;
use Drupal\tmgmt\Entity\Job;
use Drupal\tmgmt\Entity\RemoteMapping;
use Drupal\tmgmt\Entity\Translator;
use Drupal\tmgmt\JobItemInterface;
use Drupal\tmgmt\TMGMTException;
use Drupal\tmgmt\TranslatorPluginBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Component\Render\FormattableMarkup;
use Drupal\tmgmt\TranslatorInterface;
use Drupal\tmgmt\JobInterface;
use Drupal\tmgmt\Entity\JobItem;
use Drupal\tmgmt\Translator\AvailableResult;
use SoapClient;
use SoapHeader;
use SoapFault;

/**
 * translate plus translator plugin.
 *
 * @TranslatorPlugin(
 *   id = "translateplus",
 *   label = @Translation("translate plus"),
 *   description = @Translation("translate plus translator service."),
 *   ui = "Drupal\tmgmt_translateplus\TranslatePlusTranslatorUi"
 * )
 */
class TranslatePlusTranslator extends TranslatorPluginBase implements ContainerFactoryPluginInterface {

  /**
   * Translation service URL.
   *
   * @var string
   */
  protected $apiURL = 'https://iplus.translateplus.com/TPWSv2/TPWS.asmx?wsdl';
  //protected $apiURL = 'https://iplus.translateplus.com/TPWSv2/TPWS.asmx';


  /**
   * Translation service sandbox URL.
   *
   * @var string
   */
  protected $sandboxURL = 'https://iplusQA.translateplus.com/TPWSv2/TPWS.asmx?wsdl';
  //protected $sandboxURL = 'http://iplusqa.translateplus.com/tpwsv2/TPWS.asmx';


  /**
   * SoapClient
   */
  private $soap_client;


  /**
   * Flag to trigger debug watchdog logging of requests.
   *
   * Use variable_set('tmgmt_translateplus_debug', TRUE); to toggle debugging.
   *
   * @var bool
   */
  private $debug = FALSE;


  /**
   * The database.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;

  /**
   * Constructs a TranslatePlusTranslator object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param array $plugin_definition
   *   The plugin implementation definition.
   */
  public function __construct(array $configuration, $plugin_id, array $plugin_definition, Connection $connection) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->connection = $connection;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $connection = \Drupal::database();
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $connection
    );
  }

  /**
   * Overrides TMGMTDefaultTranslatorPluginController::checkAvailable().
   */
  public function checkAvailable(TranslatorInterface $translator) {
    if ($translator->getSetting('api_account_guid')) {
      return AvailableResult::yes();
    }
    return AvailableResult::no(t('@translator is not available. Make sure it is properly <a href=:configured>configured</a>.', [
      '@translator' => $translator->label(),
      ':configured' => $translator->toUrl(),
    ]));
  }


  /**
   * Overrides TMGMTDefaultTranslatorPluginController::hasCheckoutSettings().
   */
  public function hasCheckoutSettings(JobInterface $job) {
    return FALSE;
  }


  /**
   * Execute a SOAP request against the translate plus API.
   *
   * @param Translator $translator
   *   The translator entity to get the settings from.
   * @param string $service
   *   The service that should be requested from the API, for example Version or
   *   GetLanguagesList.
   * @param array $params
   *   (Optional) Array of SOAP parameters.
   * @param bool $cache
   *   (Optional) Number of seconds to cache data.
   * @param bool $force_cache_refresh
   *   (Optional) Resend request regardless of cache status, updating new
   *   cache entry if releevant.
   *
   * @return object
   *   The response object returned by SOAP client.
   */
  public function doRequest(Translator $translator, $service, array $params = array(), $cache = NULL, $force_cache_refresh = FALSE) {
    // If caching is enabled, check for valid entries first.
    if ($cache) {
      $cache_key = 'tmgmt_tplus:' . ($translator->getSetting('use_sandbox') ? 'sandbox' : 'api') . ':' . $service . ':' . md5(serialize($params));

      // Only check cache if not forcing refresh.
      if (!$force_cache_refresh) {
        $cached = \Drupal::cache()->get($cache_key);

        // Not expired, use cached request value.
        if ($cached && Drupal::time()->getRequestTime() < $cached->expire) {
          return $cached->data;
          //return simplexml_load_string($cached->data);
        }
      }
    }


    // Build param array to pass to translate plus service.
    $params = array(
      'GUIDString' => $translator->getSetting('api_account_guid'),
    ) + $params;

    // Cache either expired, force-refreshed or not enabled - proceed with API requests.
    $is_fault = FALSE;

    $url = $translator->getSetting('use_sandbox') ? $this->sandboxURL : $this->apiURL;


    if (empty($this->soap_client)) {
      try {
        $this->soap_client = new SoapClient(
          $url,
          array(
            'trace' => 1,
            'exceptions' => 1,
          )
        );
      }
      // Request failed.
      catch (SoapFault $fault) {
        $is_fault = TRUE;
      }
    }


    if ($is_fault !== TRUE) {
      try {

        // Add User-Agent string in request header to allow translate plus identification of Drupal-based requests.
        $header = new SoapHeader('http://www.translateplus.com/TPWS', 'User-Agent', 'Drupal TMGMT translate plus', FALSE);
        $this->soap_client->__setSoapHeaders($header);

        // Call soap service.
        $result = $this->soap_client->__soapCall($service, array($service => $params));

        // No need to store the encoded data in the dblog.
        if (isset($params['SourceFileBytesData'])) {
          $params['SourceFileBytesData'] = '[[BASE64 ENCODED DATA OMITTED FROM LOG]]';
        }

        if ($translator->getSetting('enable_debugging')) {
          // Log request.
          \Drupal::logger('tmgmt_translateplus')->info('API request completed.<br /><pre>@data</pre>', array(
            '@service' => $service,
            '@data' => print_r(array(
              'url' => $url,
              'service' => $service,
              'params' => $params,
              'response' => $result,
            ), TRUE))
          );
        }

        // Catch server-side soap fault.
        if (is_soap_fault($result)) {
          $is_fault = TRUE;
          $fault = $result;
        }
        // If result object is a simple wrapper for actual resultset, just return the resultset.
        elseif (count((array) $result) === 1 && isset($result->{$service . 'Result'})) {
          $result = $result->{$service . 'Result'};
        }
      }
      // Catch client-side soap fault.
      catch (SoapFault $fault) {
        $is_fault = TRUE;
      }
    }

    // If either server- or clientside SOAP fault returned, trip error handling.
    if ($is_fault) {

      // Return Fault values for debugging.
      $result = new \stdClass();
      $result->isFault = TRUE;
      $result->fault = $fault;
      $result->url = $url;
      $result->service = $service;
      $result->params = $params;


      // Log error.
      // Debugging enabled
      if ($translator->getSetting('enable_debugging')) {
        \Drupal::logger('tmgmt_translateplus')->error('SoapFault on "@service": %error.<br /><pre>@data</pre>', array(
          '@service' => $service,
          '%error' => (string) $fault->faultstring,
          '@data' => print_r(array(
            'url' => $url,
            'service' => $service,
            'params' => $params,
          ), TRUE))
        );
      } 
      // Debugging disabled
      else {
        \Drupal::logger('tmgmt_translateplus')->error('Request for service %service failed with error %error', array('%service' => $service, '%error' => $fault->faultstring));
      }
      
      // Display error
      \Drupal::messenger()->addMessage(t('Request for service %service failed with error %error', array('%service' => $service, '%error' => $fault->faultstring)));

    }


    if ($cache && is_int($cache) && empty($result->isFault)) {
      \Drupal::cache()->set($cache_key, $result, Drupal::time()->getRequestTime() + $cache);
    }

    return $result;


  }



  /**
   * Returns translate plus service version.
   *
   * @return string
   *   Returns array of the supported languages.
   *   The key is drupal-style language code,
   *   the value - translate plus-style language code.
   */
  public function getVersion(Translator $translator) {
    $response = $this->doRequest($translator, 'Version');
    return (!empty($response->isFault)) ? FALSE : (string) $response;
  }

  /**
   * Returns array of supported currencies by translate plus service.
   *
   * @return array
   *   Returns array of the supported currencies.
   *   The key is the translate plus internal currency code,
   *   the value is human-readable text value
   */
  private function getCurrenciesList(Translator $translator) {
    $response = $this->doRequest(
      $translator,
      'GetCurrenciesList',
      array(),
      TMGMT_TRANSLATEPLUS_API_CACHE_EXPIRE
    );
    return (!empty($response->isFault)) ? FALSE : $response;
  }

  /**
   * Get available translate plus currencies.
   */
  private function getDefaultCurrencies() {
    return array(
      4 => 'Pounds Sterling (GBP)',
      5 => 'US Dollars (USD)',
      6 => 'Euro (EUR)',
      7 => 'Canadian Dollars (CAD)',
      8 => 'Denmark Kroner (DKK)',
      9 => 'Norway Kroner (NOK)',
      10 => 'Sweden Kronor (SEK)',
      11 => 'Swiss Francs (CHF)',
    );
  }

  /**
   * Returns key => value array of supported currencies by translate plus service.
   *
   * @return array
   *   Returns array of the supported currencies.
   *   The key is the translate plus internal currency code,
   *   the value is human-readable text value
   */
  public function getCurrencies(Translator $translator) {
    $currencies = array();
    if ($results = $this->getCurrenciesList($translator)) {

      foreach ($results->TPWSCurrency as $currency) {
        $currencies[(string) $currency->ID] = $currency->Name . ' (' . $currency->Symbol . ')';
      }

    }
    if (empty($currencies)) {
      return $this->getDefaultCurrencies();
    }
    return $currencies;
  }


  /**
   * Returns array of account-specific status types from translate plus service.
   *
   * @return array
   *   Returns array of the statuses.
   *   The ID is the translate plus internal status ID,
   *   the Name is human-readable text value
   */
  private function getStatusTypesList(Translator $translator) {
    $response = $this->doRequest(
      $translator,
      'GetStatusTypesList',
      array(),
      TMGMT_TRANSLATEPLUS_API_CACHE_EXPIRE
    );
    return (!empty($response->isFault)) ? FALSE : $response;
  }

  /**
   * Return available translate plus status types.
   */
  public function getStatusList(Translator $translator) {
    $statuses = array();
    if ($results = $this->getStatusTypesList($translator)) {
      foreach ($results->TPWSItemStatus as $status) {
        $statuses[(string) $status->ID] = (string) $status->Name;
      }
    }
    return $statuses;
  }


  /**
   * Implements TranslatorPluginControllerInterface::requestTranslation().
   */
  public function requestTranslation(JobInterface $job) {
    $translator = $job->getTranslator();

    global $base_url;
    // Set up the ExtensibleData parameter.
    $ext_params = array(
      'base_url' => $base_url,
      'tmgmt_tjid' => $job->id(),
    );

    if ($translator->getSetting('include_user_data')) {
      $user = \Drupal\user\Entity\User::load(\Drupal::currentUser()->id());
      $ext_params += array(
        'user_id' => $user->id(),
        'user_accountname' => $user->getAccountName(),
        'user_displayname' => $user->getDisplayName(),
        'user_email' => $user->getEmail(),
      );
      $user_full_name = $translator->getSetting('user_full_name');
      if (!empty($user_full_name)) {
        $ext_params['user_full_name'] = token_replace($translator->getSetting('user_full_name'), array('user' => $user));
      }
    }

    // Basic-use translation job method.
    $service = 'CreateTranslationOrder';


    //$job->getSetting('notes');
    // Shared parameters for both HT and MT.
    $deadline = is_null($job->getSetting('due_date'))? '+1 week' : $job->getSetting('due_date');
    $params = array(
      'JobName' => $job->label(),
      'DeadlineDateTime' => \Drupal::service('date.formatter')->format(strtotime($deadline), 'custom', 'c'),
      //'SourceFileBytesData' => base64_encode($data),
      'SourceFileName' => '',
      'SourceFileBytesData' => '',
      'SourceLang' => $job->getRemoteSourceLanguage(), // $job->source_language,
      'TargetLangs' => array(
        $job->getRemoteTargetLanguage(), // $job->target_language,
      ),
      'PurchaseOrderNumber' => '',
      'CurrencyID' => $translator->getSetting('currency'),
      'CustomerNotes' => '',
    );

    // Optional parameters.
    if (!empty($job->getSetting('purchase_order'))) {
      $params['PurchaseOrderNumber'] = $job->getSetting('purchase_order');
    }
    if (!empty($job->getSetting('comments'))) {
      $params['CustomerNotes'] = $job->getSetting('comments');
    }

    // Additional settings for machine translation.
    if ($job->getSetting('translation_method') == 'mt') {

      // Use alternative API method to create MT job.
      $service = 'CreateMTOrder';

      // Additional params apply only to MT.
      $params += array(
        'TranslationMemoryUsage' => $job->getSetting('mt_use_tm'),
        'ApplyHumanPostEdit' => $job->getSetting('mt_human_postedit'),
      );
    }

    // Fetch already submitted job parts.
    $remote_parts = $this->getSubmittedRemotes($job);

    // Get job data parts and send one request for each.
    $data_split = $this->getDataParts($job);



    $batch_data = array();
    foreach ($data_split as $part_key => $part) {

      // Check if this part has already been submitted.
      if (array_key_exists($part_key, $remote_parts) && $remote_parts[$part_key] == count($part['items'])) {
        continue;
      }

      // Collapse and wrap data.
      $part_data = $this->prepareDataForSending($part['data']);

      // Change order details per part.
      $params['JobName'] = $job->id() . '_' . $part_key;
      $params['SourceFileName'] = $job->id() . '_' . $part_key . '.xml';
      $params['SourceFileBytesData'] = base64_encode($part_data);

      

      // Change extensible data per part
      $part_hash = tmgmt_translateplus_hash($job->id(), $part_key);
      $ext_params['callback_url'] = Url::fromRoute('tmgmt_translateplus.callback')->setOptions(
        array(
          'absolute' => TRUE,
          'query' => array(
            'key' => $part_hash, //$job->tjid
          ),
        )
      )->toString();

      // Assemble extensible data into xml string
      $ext_data_str = array();
      $ext_data_val = array();
      foreach ($ext_params as $k => $v) {
        $ext_data_str[] = $k . '=":' . $k . '"';
        $ext_data_val[':' . $k] = $v;
      }
      $ext_data = (string) new FormattableMarkup('<job ' . implode(' ', $ext_data_str) . '></job>', $ext_data_val);

      $params['ExtensibleData'] = $ext_data;



      $batch_data[] = array(
        'part_key' => $part_key,
        'part_hash' => $part_hash,
        'part' => $part,
        'params' => $params,
      );

    }

    
    if (!empty($batch_data)) {
      $batch = array(
        'operations' => array(
          array(
            'tmgmt_translateplus_batch_submit_translations',
            array(
              $job,
              $service,
              $batch_data,
            ),
          ),
        ),
        'finished' => 'tmgmt_translateplus_batch_submit_translations_complete',
        'title' => t('Submitting data to translate plus'),
        'error_message' => t('An error occurred while submitting translation jobs to translate plus'),
        'file' => \Drupal::service('extension.path.resolver')->getPath('module', 'tmgmt_translateplus') . '/tmgmt_translateplus.module',
      );
      batch_set($batch);
    }
    

  }

  /**
   * Send single translation job part to translate plus.
   */
  public function requestTranslationProcessPart(JobInterface $job, $service, $part) {

    // Send translation request to API.
    $response = $this->doRequest(
        $job->getTranslator(),
        $service,
        $part['params']
      );

    // Request returned OK.
    //if (!isset($response->isFault) && !is_soap_fault($response)) {
    if (empty($response->isFault)) {
      // Register tmgmt_remote mappings per item.
      foreach ($part['part']['items'] as $tjiid => $item) {
        $item->addRemoteMapping($part['part_key'], $response, ['remote_identifier_2' => $part['part_hash']]);
      }

      // Return ID.
      return $response;
    }
    // Request returned fault.
    else {
      return FALSE;
    }

  }

  /**
   * Complete submission to translate plus.
   */
  public function requestTranslationProcessComplete(JobInterface $job, $results) {

    // All requests returned OK.
    if (empty($results['error']) && count($results['success']) == $results['total']) {

      // Remote mappings
      $remote_ids = [];
      if ($remotes = $job->getRemoteMappings()) {
        foreach($remotes as $r) {
          $remote_ids[] = $r->getRemoteIdentifier1();
        }
      }

      $job->submitted('The translation job has been submitted to translate plus as job IDs %translation_id.', array('%translation_id' => join(", ", array_unique($remote_ids))));

    }

    // All requests returned error.
    elseif (count($results['error']) == $results['total']) {
      $job->rejected('Could not submit job to translate plus.', array(), 'error');
    }

    // Requests returned at least one error.
    else {
      $job->rejected('Could not submit @count job items to translate plus.', array('@count' => count($results['error'])), 'error');
    }

  }

  /**
   * Returns array of job-specific items from translate plus service.
   *
   * @param Translator $translator
   *   The translator entity to get the settings from.
   * @param JobInterface $job
   *   The job to get items for.
   *
   * @return array
   *   Returns array of the job items
   *   The ID is the translate plus internal status ID,
   *   the Name is human-readable text value
   */
  public function getJobItemsOfOrder(Translator $translator, JobInterface $job) {
    $response = $this->doRequest(
      $translator,
      'GetJobItemsOfOrder',
      array(
        'JobOrderID' => $job->reference,
      )
    );
    return (!empty($response->isFault)) ? FALSE : $response;
  }

  /**
   * Get translations jobs with specific status.
   */
  public function getJobItemsWithStatus(Translator $translator, $status) {
    $response = $this->doRequest(
      $translator,
      'GetJobItemsWithStatus',
      array(
        'Status' => $status,
      )
    );
    return (!empty($response->isFault)) ? FALSE : $response;
  }

  /**
   * Receives translation file data from translate plus.
   */
  private function collectFile(Translator $translator, $tplus_tjid, $file_number = 1) {
    $response = $this->doRequest(
      $translator,
      'CollectFile',
      array(
        'JobItemID' => $tplus_tjid,
        'FileNumber' => $file_number,
      )
    );

    // Debug
    if ($translator->getSetting('enable_debugging')) {
      \Drupal::logger('tmgmt_translateplus')->info("Collect file request <pre>@data</pre>", array(
        '@data' => var_export($response, true),
      ));
    }
    return $response;
  }

  /**
   * Confirms file collection with translate plus.
   */
  private function confirmFileCollection(Translator $translator, $tplus_tjid, $file_number = 1) {
    $response = $this->doRequest(
      $translator,
      'ConfirmFileCollection',
      array(
        'JobItemID' => $tplus_tjid,
        'FileNumber' => $file_number,
      )
    );
    return $response;
  }

  /**
   * Triggers file collection from translate plus and stores translated items.
   */
  public function retrieveTranslation(JobInterface $job, $tplus_tjid) {

    $translator = $job->getTranslator();
    $response = $this->collectFile($translator, $tplus_tjid);

    // Translation data received from translate plus.
    if (empty($response->isFault)) {
      $data = $this->parseTranslationData($response->FileByteData);

      $job->addMessage('The translation for job ID %translation_id has been received from translate plus.', array('%translation_id' => $tplus_tjid));
      $job->addTranslatedData($data);

      // Confirm file collection.
      if (!$translator->getSetting('collection_disable_confirmation')) {
        $collect_response = $this->confirmFileCollection($translator, $tplus_tjid);
      }

      return TRUE;
    }

    // Error received from translate plus.
    else {
      $job->addMessage('Could not get translation job ID %translation_id from translate plus. Message error: %error', array('%translation_id' => $tplus_tjid, '%error' => $response->fault->faultstring), 'error');
      return FALSE;
    }

  }


  /**
   * Split translation job into separate parts depending on job settings.
   */
  public function getDataParts(JobInterface $job) {
    $mode = !empty($job->getSetting('split_mode')) ? $job->getSetting('split_mode') : 'source';
    $data = array();

    // No splitting into parts
    if ($mode == 'none') {
      $data = array(
        'all' => array(
          'items' => $job->getItems(),
          'data' => $job->getData(),
          'word_count' => $job->getWordCount(),
        ),
      );
      return $data;
    }

    if ($mode == 'sequence') {
      $items = $job->getItems();

      $data = $this->getDataSequence($job, $items, 'part');
      return $data;
    }

    // Fetch word counts per plugin and item type, used for splitting up data.
    $result = $this->connection->query("SELECT plugin, item_type, sum(word_count) word_count FROM {tmgmt_job_item} WHERE tjid=:tjid GROUP BY plugin, item_type", array(':tjid' => $job->id()));

    $split_mode_items_bundles = array('content', 'taxonomy_term');

    $plugin = '';
    $structure = array();
    if ($result) {
      while ($row = $result->fetchAssoc()) {

        // Set current plugin marker.
        $plugin = $row['plugin'];

        // Init new plugin structure array.
        if (!isset($structure[$plugin])) {
          $structure[$plugin] = array(
            'item_types' => array(),
            'split_mode' => NULL,
            'word_count' => 0,
          );
        }

        // Default item-type settings.
        $structure[$plugin]['item_types'][$row['item_type']] = array(
          'item_type' => $row['item_type'],
          'split_mode' => NULL,
          'word_count' => $row['word_count'],
        );

        // Do additional item-type processing.
        if ($row['word_count'] > TMGMT_TRANSLATEPLUS_WORDS_PER_REQUEST) {
          // Default item-type splitting mode.
          $split_mode = 'sequence';

          // Nodes and taxonomy_terms are split by bundle first.
          if ($plugin == 'entity' && in_array($row['item_type'], $split_mode_items_bundles)) {
            $split_mode = 'bundle';
          }

          // Set item-type split mode.
          $structure[$plugin]['item_types'][$row['item_type']]['split_mode'] = $split_mode;
        }

        // Increment total word-count per plugin.
        $structure[$plugin]['word_count'] += $row['word_count'];

        // Should we split plugin into multiple requests?
        if ($structure[$plugin]['word_count'] > TMGMT_TRANSLATEPLUS_WORDS_PER_REQUEST) {

          // Default behavior is a sequential split.
          $split_mode = 'sequence';

          // Entities are special - begin by splitting them into their entity types.
          if ($plugin == 'entity') {
            $split_mode = 'item_type';
          }

          // Set plugin split mode.
          $structure[$plugin]['split_mode'] = $split_mode;
        }

      }
    }

    // If split_mode_items is enabled, override entity plugin settings for node and taxonomy_term.
    if (!empty($job->getSetting('split_mode_items')) && $job->getSetting('split_mode_items') && array_key_exists('content', $structure)) {
      $structure['content']['split_mode'] = 'item_type';
      foreach ($split_mode_items_bundles as $bundle) {
        if (!empty($structure['entity']['item_types'][$bundle])) {
          $structure['content']['item_types'][$bundle]['split_mode'] = 'bundle';
        }
      }
    }

    // Run through sources and item types, splitting up items according to parameters calculated above.
    $data = array();
    foreach ($structure as $source_key => $source) {
      if ($source['split_mode'] == 'item_type' && !empty($source['item_types'])) {
        foreach ($source['item_types'] as $type_key => $type) {

          // Use short name, due to filename length restrictions with translate plus.
          $type_key_safe = $type_key;
          if ($type_key == 'taxonomy_term') {
            $type_key_safe = 'term';
          }

          // Fetch source and item-type specific job items.
          $items = $job->getItems(array('plugin' => $source_key, 'item_type' => $type_key));

          // Special processing when splitting into bundles.
          if ($type['split_mode'] == 'bundle') {

            $bundles = array();
            foreach ($items as $tjiid => $item) {
              $wrapper = entity_metadata_wrapper($type['item_type'], $item->item_id);
              $bundles[$wrapper->getBundle()][$tjiid] = $item;
            }

            foreach ($bundles as $bundle_key => $bundle_items) {
              $bundle_key_safe = drupal_substr($bundle_key, 0, 20);
              if (!empty($job->getSetting('split_mode_items')) && $job->getSetting('split_mode_items')) {
                foreach ($bundle_items as $tjiid => $item) {
                  $data[$type_key_safe . '_' . $bundle_key_safe . '_item_' . $tjiid] = array(
                    'items' => array(
                      $tjiid => $item,
                    ),
                    'data' => array(
                      $tjiid => $item->getData(),
                    ),
                    'word_count' => $item->word_count,
                  );
                }
              }
              else {
                $data += $this->getDataSequence($job, $bundle_items, $type_key_safe . '_' . $bundle_key_safe);
              }

            }
          }

          // Default to sequential split for items.
          else {
            $data += $this->getDataSequence($job, $items, $source_key . '_' . $type_key_safe);
          }

        }
      }

      else {

        // Fetch source-specific job items.
        $items = $job->getItems(array('plugin' => $source_key));
        $data += $this->getDataSequence($job, $items, $source_key);
      }

    }

    return $data;
  }

  /**
   * Split job items sequentially.
   */
  protected function getDataSequence(JobInterface $job, $items, $prefix = NULL) {
    $data = array();

    $i = 0;

    foreach ($items as $tjiid => $item) {
      $index = ($prefix) ? $prefix . '_' . str_pad($i + 1, 3, '0', STR_PAD_LEFT) : str_pad($i + 1, '0', STR_PAD_LEFT);

      // Max words per request exceeded - init new request.
      if (!empty($data[$index]) && $data[$index]['word_count'] + $item->getWordCount() > TMGMT_TRANSLATEPLUS_WORDS_PER_REQUEST) {
        $i++;
        $index = ($prefix) ? $prefix . '_' . str_pad($i + 1, 3, '0', STR_PAD_LEFT) : str_pad($i + 1, '0', STR_PAD_LEFT);
      }

      if (!isset($data[$index])) {

        $data[$index] = array(
          'items' => array(),
          'data' => array(),
          'word_count' => 0,
        );
      }

      $data[$index]['items'][$tjiid] = $item;
      $data[$index]['data'] += $job->getData(array($tjiid));
      $data[$index]['word_count'] += $item->getWordCount();
    }
    return $data;
  }

  /**
   * Prepares data to be send to translate plus service and returns XML string.
   *
   * @param array $data
   *   Data to prepare for XML.
   *
   * @return string
   *   XML string for sending to the translator service.
   */
  protected function prepareDataForSending(array $data) {
    //$data = array_filter(tmgmt_flatten_data($data), '_tmgmt_filter_data');

    // Pull the source data array through the job and flatten it.
    $data = \Drupal::service('tmgmt.data')->filterTranslatable($data);

    $items = '';
    foreach ($data as $key => $value) {
      $items .= str_replace(array('@key', '@text'), array($key, $value['#text']), '<item key="@key"><text type="text/html"><![CDATA[@text]]></text></item>');
    }
    return '<items>' . $items . '</items>';
  }

  /**
   * Parses received translation from translate plus and returns unflatted data.
   *
   * @param string $data
   *   Base64 encode data, received from translate plus.
   *
   * @return array
   *   Unflatted data.
   */
  protected function parseTranslationData($data) {
    $data = base64_decode($data);
    $items = simplexml_load_string($data);
    $data = array();

    foreach ($items->item as $item) {
      $key = (string) $item['key'];
      $data[$key]['#text'] = (string) $item->text;
    }

    return \Drupal::service('tmgmt.data')->unflatten($data);
  }

  /**
   * Fetch already submitted job parts.
   */
  protected function getSubmittedRemotes(JobInterface $job) {
    $result = $this->connection->query("SELECT count(tjiid) items, data_item_key FROM {tmgmt_remote} WHERE tjid=:tjid GROUP BY data_item_key", array(':tjid' => $job->id()));
    $part_keys = array();
    if ($result) {
      while ($row = $result->fetchAssoc()) {
        $part_keys[$row['data_item_key']] = $row['items'];
      }
    }
    return $part_keys;
  }

  /**
   * Return XML structure of submittable parts for review.
   */
  public function reviewPreparedData(JobInterface $job) {
    $data = $this->getDataParts($job);
    $xml = array();
    foreach ($data as $part_key => $part) {
      $xml[$part_key] = $this->prepareDataForSending($part['data']);
    }
    return $xml;
  }

}


