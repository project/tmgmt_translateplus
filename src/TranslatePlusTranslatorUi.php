<?php

/**
 * @file
 * Contains \Drupal\tmgmt_google\TranslatePlusTranslatorUi.
 */

namespace Drupal\tmgmt_translateplus;

use Drupal;
use Drupal\tmgmt\TranslatorPluginUiBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\tmgmt\JobItemInterface;
use Drupal\tmgmt\JobInterface;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Component\Utility\Xss;

/**
 * translate plus translator UI.
 */
class TranslatePlusTranslatorUi extends TranslatorPluginUiBase {

  /**
   * Overrides TMGMTDefaultTranslatorUIController::pluginSettingsForm().
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);


    /** @var \Drupal\tmgmt\TranslatorInterface $translator */
    $translator = $form_state->getFormObject()->getEntity();
    $plugin = $translator->getPlugin();


    $form['api_account_guid'] = array(
      '#type' => 'textfield',
      '#title' => t('translate plus API GUID'),
      '#description' => t('Please enter your GUID for the translate plus API service.'),
      '#default_value' => $translator->getSetting('api_account_guid'),

    );
// '#description' => t('Please enter your GUID for the translate plus API service. This setting can be managed in code by adding it to your settings.php file as %variable_name.', array('%variable_name' => '$conf[\'translateplus_api_account_guid\']')),
/*
    if (!empty($conf['translateplus_api_account_guid'])) {
      $form['api_account_guid']['#disabled'] = TRUE;
      $form['api_account_guid']['#description'] = t('The api GUID is set in your settings.php file and cannot be changed here.');
    }
*/

    $form['currency'] = array(
      '#type' => 'select',
      '#title' => t('Your default currency'),
      '#description' => t('Select your preferred billing currency.'),
      '#options' => $plugin->getCurrencies($translator),
      '#default_value' => $translator->getSetting('currency'),
    );


    $form['enable_development'] = array(
      '#type' => 'checkbox',
      '#title' => t('Enable advanced settings?'),
      '#description' => t('If enabled, further settings will be displayed, allowing options for debugging and disabling part of the translation process.'),
      '#default_value' => $translator->getSetting('enable_development'),
    );

    $form['collection_method'] = array(
      '#type' => 'select',
      '#title' => t('File collection method'),
      '#description' => t('How should completed translations be collected from translate plus?'),
      '#options' => array(
        'default' => t('Cron and callback'),
        'cron' => t('Cron only'),
        'callback' => t('Callback only'),
        'disabled' => t('Disable file collection'),
      ),
      '#default_value' => $translator->getSetting('collection_method'),
      '#states' => array(
        'visible' => array(
          ':input[name="settings[enable_development]"]' => array('checked' => TRUE),
        ),
      ),
    );

    $form['collection_disable_confirmation'] = array(
      '#type' => 'checkbox',
      '#title' => t('Disable confirmation of file collection'),
      '#description' => t('When files have been successfully collected from translate plus, the system sends a confirmation, and the translation status with translate plus is set to "collected". Disabling the confirmation will keep the "ready for collection" status, meaning that Drupal will attempt to collect it on every cron run. Only check this, if you know what you\'re doing.'),
      '#default_value' => $translator->getSetting('collection_disable_confirmation'),
      '#states' => array(
        'visible' => array(
          ':input[name="settings[enable_development]"]' => array('checked' => TRUE),
        ),
      ),
    );

    $form['enable_debugging'] = array(
      '#type' => 'checkbox',
      '#title' => t('Enable debugging'),
      '#description' => t('Increase logging level for debugging purposes.'),
      '#default_value' => $translator->getSetting('enable_debugging'),
      '#states' => array(
        'visible' => array(
          ':input[name="settings[enable_development]"]' => array('checked' => TRUE),
        ),
      ),
    );

    $form['use_sandbox'] = array(
      '#type' => 'checkbox',
      '#title' => t('Use the sandbox'),
      '#default_value' => $translator->getSetting('use_sandbox'),
      '#description' => t('Check to use the testing environment.'),
      '#states' => array(
        'visible' => array(
          ':input[name="settings[enable_development]"]' => array('checked' => TRUE),
        ),
      ),
    );

    $form['include_user_data'] = array(
      '#type' => 'checkbox',
      '#title' => t('Include user details?'),
      '#description' => t('If enabled, basic user details (id, name and email) will be included with each job submission.'),
      '#default_value' => $translator->getSetting('include_user_data'),
    );
/*
    if (module_exists('token')) {
      $form['user_full_name'] = array(
        '#type' => 'textfield',
        '#title' => t("User's full name"),
        '#description' => t("Enter relevant tokens to include as the user's full name. Leave blank to omit full name from submission."),
        '#default_value' => $translator->getSetting('user_full_name'),
        '#states' => array(
          'visible' => array(
            ':input[name="settings[include_user_data]"]' => array('checked' => TRUE),
          ),
        ),
      );
      $form['tokens'] = array(
        '#theme' => 'token_tree_link',
      // The token types that have specific context. Can be multiple token types like 'term' and/or 'user'.
        '#token_types' => array('user'),
      // A boolean TRUE or FALSE whether to include 'global' context tokens like [current-user:*] or [site:*]. Defaults to TRUE.
        '#global_types' => FALSE,
      // A boolean whether to include the 'Click this token to insert in into the the focused textfield' JavaScript functionality. Defaults to TRUE.
        '#click_insert' => TRUE,
        '#states' => array(
          'visible' => array(
            ':input[name="settings[include_user_data]"]' => array('checked' => TRUE),
          ),
        ),
      );
    }

*/

    $form += parent::addConnectButton();
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::validateConfigurationForm($form, $form_state);
    /** @var \Drupal\tmgmt\TranslatorInterface $translator */

    $translator = $form_state->getFormObject()->getEntity();
    $plugin = $translator->getPlugin();



    if (!tmgmt_translateplus_api_guid($translator) && !isset($form_state->settings['api_account_guid'])) {
      $form_state->setErrorByName('settings][api_account_guid', t('A translate plus API GUID is required for submitting translation jobs.'));  
    }

    // File collection completely disabled.
    if ($translator->getSetting('collection_method') == 'disabled') {
      \Drupal::messenger()->addMessage(t('File collection is completely disabled. Completed files will be never be collected.'), MessengerInterface::TYPE_ERROR);
    }

    // File collection confirmation is disabled.
    if ($translator->getSetting('collection_disable_confirmation')) {
      \Drupal::messenger()->addMessage(t('File collection confirmation is disabled. Completed translations will never be marked as collected, and will be processed on every collection run.'), MessengerInterface::TYPE_ERROR);
    }

    // Using the sandbox server.
    if ($translator->getSetting('use_sandbox')) {
      \Drupal::messenger()->addMessage(t('Using the API sandbox. Disable the sandbox to submit live translation jobs.'), MessengerInterface::TYPE_ERROR);
    }

  }



  /**
   * {@inheritdoc}
   */
  public function checkoutSettingsForm(array $form, FormStateInterface $form_state, JobInterface $job) {

    $translator = $job->getTranslator();
    $plugin = $translator->getPlugin();

//     $parts = $plugin->getDataParts($job);
// kint($plugin->getDataParts($job), 'parts');

    // Get the job word count.
    //$word_count = tmgmt_job_statistic($job, 'word_count');
    $word_count = $job->getWordCount();
    // 1 day per 1000 words plus two days turnaround.
    $suggested_days = max(1, ceil($word_count / 1000)) + 2;
    $suggested_date = strtotime('+' . $suggested_days . ' days');

    $settings = array(
      'translation_method' => array(
        '#title' => t('Translation method'),
        '#type' => 'select',
        '#description' => t('<em>Human translations</em> are created by professional, qualified translators with the relevant experience and skill set appropriate to our customers’ requirements. <em>Machine translations</em> are fast, automated translations, without the involvement of a human translator. For most jobs, you should choose <em>human translation</em>.'),
        '#options' => array(
          'ht' => t('Human translation'),
          'mt' => t('Machine translation'),
        ),
        '#default_value' => !empty($job->getSetting('translation_method')) ? $job->getSetting('translation_method') : 'ht',
      ),

      'mt_use_tm' => array(
        '#title' => t('Use translation memory'),
        '#type' => 'checkbox',
        '#description' => t('When enabled, content will be passed through your default translation memory first, to look for matches before passing the content to the machine translation engine.'),
        '#default_value' => !empty($job->getSetting('mt_use_tm')) ? $job->getSetting('mt_use_tm') : 1,
        '#states' => array(
          'visible' => array(
            ':input[name="settings[translation_method]"]' => array('value' => 'mt'),
          ),
        ),
      ),
      'mt_human_postedit' => array(
        '#title' => t('Enable human post-editing'),
        '#type' => 'checkbox',
        '#description' => t('Enabling human edits will typically result in a longer turnaround time and higher costs, but a higher translation quality; disabling will typically result in a shorter turnaround time and lower costs, but a lesser translation quality.'),
        '#default_value' => !empty($job->getSetting('mt_human_postedit')) ? $job->getSetting('mt_human_postedit') : 0,
        '#states' => array(
          'visible' => array(
            ':input[name="settings[translation_method]"]' => array('value' => 'mt'),
          ),
        ),
      ),
      'due_date' => array(
        '#title' => t('Due date'),
        '#type' => 'datelist',
        '#description' => t('@suggested_days days suggested deadline, based on one day per 1000 words plus two days turnaround. Please be aware that setting this will not guarantee delivery by the specified date & time.', array('@suggested_days' => $suggested_days)),
        '#default_value' => (!empty($job->getSetting('due_date')) && strtotime($job->getSetting('due_date')) > Drupal::time()->getRequestTime()) ? $job->getSetting('due_date') : DrupalDateTime::createFromTimestamp($suggested_date),
        '#date_increment' => 15,
        '#date_year_range' => '0:+3',
      ),
      'purchase_order' => array(
        '#title' => t('Purchase order number'),
        '#type' => 'textfield',
        '#description' => t('An optional purchase order (PO) number which for some customers is an essential requirement of placing an order. If a PO number is provided, it will ultimately appear on our invoices.'),
        '#default_value' => !empty($job->getSetting('purchase_order')) ? $job->getSetting('purchase_order') : '',
      ),
      'comments' => array(
        '#title' => t('Comments'),
        '#type' => 'textarea',
        '#description' => t('Human-readable notes about this request which might be meaningful to our project managers and/or translators. If using machine translation, please be aware that unless you are requesting human post-editing, machine translation is designed to be immediate, and therefore these notes would not normally be looked at or acted on during a fully automated translation process.'),
        '#default_value' => !empty($job->getSetting('comments')) ? $job->getSetting('comments') : '',
      ),

      'split_mode' => array(
        '#title' => t('Large job handling method'),
        '#description' => t('In case of large jobs, the content will be split across several translate plus submissions to avoid issues with time-out and translation concurrency. The groupings will also be reflected in the translate plus management interface.'),
        '#type' => 'radios',
        '#options' => array(
          'none' => t('Do not split. Force all content to be submitted as one request.'),
          'sequence' => t('Sequential. Will result in a straight-forward sequence of content, matching order of items in the translation job.'),
          'source' => t('By source. Items will be grouped by content source, type and sequentially, as needed.'),
        ),
        '#default_value' => !empty($job->getSetting('split_mode')) ? $job->getSetting('split_mode') : 'source',
      ),
      'split_mode_items' => array(
        '#disabled' => true,
        '#title' => t('Separate nodes and taxonomy terms into distinct submissions per item'),
        '#type' => 'checkbox',
        '#description' => t('If checked, each node or term item will be submitted as a separate item, rather than a sequential split, to translate plus. This is an advanced option, primarily used for when very granular control in translate plus is required.'),
        '#default_value' => !empty($job->getSetting('split_mode_items')) ? $job->getSetting('split_mode_items') : 0,
        '#states' => array(
          'visible' => array(
            ':input[name="settings[split_mode]"]' => array('value' => 'source'),
          ),
        ),
      ),

    );

    return $settings;
  }


  /**
   * {@inheritdoc}
   */
  public function checkoutInfo(JobInterface $job) {
    $tuid = $job->getSetting('translator');

    // Remote mappings
    if ($remotes = $job->getRemoteMappings()) {
      $tp_jobs = [];
      $tp_links = [];
      foreach($remotes as $r) {
        $url = \Drupal\Core\Url::fromUri('https://iplus.translateplus.com/?Redirect=ViewOrderDetails.aspx?OrderID=' . $r->getRemoteIdentifier1());
        $tp_jobs[$r->getRemoteIdentifier1()] = [
          'reference' => $job->id() . '_' . $r->get('data_item_key')->value,
          'key' => $r->get('data_item_key')->value,
          'remote_id1' => $r->getRemoteIdentifier1(),
          'remote_id2' => $r->getRemoteIdentifier2(),
          'link' => \Drupal\Core\Link::fromTextAndUrl($r->getRemoteIdentifier1(), $url),
        ];
        $tp_links[$r->getRemoteIdentifier1()] = \Drupal\Core\Link::fromTextAndUrl($r->getRemoteIdentifier1(), $url, ['target'=>'_blank'])->toString();
      }

      $form['job_remote_id'] = array(
        '#type' => 'item',
        '#title' => t('translate plus job IDs'),
        '#markup' => join(", ", array_values($tp_links)),
        // '#markup' => '<pre>' . print_r($tp_jobs, true) . '</pre>',
      );
    }
    
    // Translation settings
    $translation_method = !empty($job->getSetting('translation_method')) ? $job->getSetting('translation_method') : "ht";
    $mt_use_tm = !empty($job->getSetting('mt_use_tm')) ? $job->getSetting('mt_use_tm') : 1;
    $mt_human_postedit = !empty($job->getSetting('mt_human_postedit')) ? $job->getSetting('mt_human_postedit') : 0;

    $form['translation_method'] = array(
      '#type' => 'item',
      '#title' => t('Translation method'),
      '#markup' => $translation_method == "ht" ? t("Human translation") : t("Machine translation"),
    );

    if ($translation_method == "mt") {

      $form['translation_method']['#markup'] .= ", ";
      $form['translation_method']['#markup'] .= $mt_use_tm ? t('translation memory enabled') : t('translation memory disabled');

      $form['translation_method']['#markup'] .= ", ";
      $form['translation_method']['#markup'] .= $mt_human_postedit ? t('tuman post-editing enabled') : t('human post-editing disabled');

    }
  
    $form['due_date'] = array(
      '#type' => 'item',
      '#title' => t('Requested due date'),
      '#markup' => !empty($job->getSetting('due_date')) ? $job->getSetting('due_date') : t('No date set'),
    );

    if (!empty($job->getSetting('purchase_order'))) {
      $form['purchase_order'] = array(
        '#type' => 'item',
        '#title' => t('Purchase order number'),
        '#markup' => Xss::filter($job->getSetting('purchase_order')),
      );
    }


    if ($job->getSetting('comments')) {
      $form['comments'] = array(
        '#type' => 'item',
        '#title' => t('Comments'),
        '#markup' => Xss::filter($job->getSetting('comments')),
      );
    }

    return $form;
  }
}
