<?php /**
 * @file
 * Contains \Drupal\tmgmt_translateplus\Controller\TranslatePlusController.
 */

namespace Drupal\tmgmt_translateplus\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\tmgmt\Entity\JobItem;
use Drupal\tmgmt\JobItemInterface;
use Drupal\tmgmt_translateplus\Plugin\tmgmt\Translator\TranslatePlusTranslator;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Route controller class for the tmgmt_translateplus module.
 */
class TranslatePlusController extends ControllerBase {

  /**
   * Provides a callback function for TranslatePlus translator.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request to handle.
   *
   * @return \Symfony\Component\HttpFoundation\Response
   *   The response to return.
   */
  public function callback(Request $request) {
    
    $key = $request->query->get('key');
    $jobs = tmgmt_translateplus_fetch_tjids_by_hash_key($key);

    // Log request.
    \Drupal::logger('tmgmt_translateplus')->info(
      "Callback URL from translate plus requested. <br /><pre>@data</pre>", 
      array(
        '@data' => print_r(array(
          'key' => $key,
          'jobs' => $jobs,
        ), TRUE)
      ),
    );
/*
    // $success = tmgmt_translateplus_retrieve_translation($job, $data['item']['ID']);

    // Debug
    $translator = $job->getTranslator();
    if ($translator->getSetting('enable_debugging')) {
      \Drupal::logger('tmgmt_translateplus')->info("Processed queue item with data <pre>@data</pre>", array(
        '@data' => var_export($data, true),
      ));
    }

*/
    $response = new Response(
      'OK',
      Response::HTTP_OK,
      ['content-type' => 'text/html']
    );
    return $response;
  }

}
